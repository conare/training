# GCP Networking and Security

## Prerequisites
* Gcloud
* Docker

## Step 1 - Docker - Multi Stage Build

1. Create a hello world app
2. Create a docker image from the app
3. Push it to GCR

## Step 2 - Subnets and Firewalls

1. Create a VPC with 3 private subnetworks as follows
    * Subnet 1 - 10.100.0.0/22
    * Subnet 2 - 10.100.4.0/22
    * Subnet 3 - 10.100.8.0/22
2. Create custom firewall rules to allow port 8080
  
  ```
  gcloud compute firewall-rules create \
  allow-8080-fwr --target-tags allow-8080 --allow tcp:8080 \
  --network gke --source-range 10.100.0.0/20
  
  ```
  
  *We will tag all our kubernetes cluster nodes with the above firewall tag*

## Step 3 - Deploy the docker image in Kubernetes

1. Authenticate to GCP using the service account created in the previous step
2. Check the service account has sufficient IAM privileges to deploy a docker image. (hint: apply to pull images from GCR)
3. Create a custom GKE cluster in 3 private subnets created under step 2 with the firewall tag (hint: use --tags flag)
4. Deploy the app in GKE under the namespace `training`
5. create a service load balancer 

## Further Labs

### Networking
1. https://codelabs.developers.google.com/codelabs/cloud-networking-101/#0
2. https://codelabs.developers.google.com/codelabs/cloud-networking-102/#0
